import { Component, ViewChild } from '@angular/core';
import { ChartComponent } from "ng-apexcharts";
import {
  ApexNonAxisChartSeries,
  ApexResponsive,
  ApexChart} from "ng-apexcharts";
export type ChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;
};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'eve';
  public input="$1000";
  @ViewChild("chart") chart: ChartComponent;
 
  public chartOptions: Partial<ChartOptions> |any;
constructor() {
  this.chartOptions = {
    series: [44, 55, 13, 43, 22,33,44,33],
    chart: {
      type: "donut"
    },
    labels: ["Travel", "Hotels", "Fuel", "Dining", "Rentals","Wholesale","Supplies","other"],
    responsive: [
      {
        breakpoint: 480,
        options: {
          chart: {
            width: 200
          },
          legend: {
            position: "bottom"
          }
        }
      }
    ]
  };
}
}
